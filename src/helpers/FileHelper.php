<?php
/**
 * @author Jakhar <jakharbek@gmail.com>
 * @package a-calc
 */

namespace jakharbek\aCalcTestLib\helpers;

use jakharbek\aCalcTestLib\exceptions\FileException;

/**
 * Class FileHelper
 * @package jakharbek\aCalcTestLib\helpers
 */
class FileHelper
{
    /**
     * @param $path
     * @return string
     * @throws FileException
     */
    public static function getData($path): string
    {
        if (!file_exists($path)) {
            throw new FileException("File is not founded");
        }
        try {
            return file_get_contents($path);
        } catch (\Exception $exception) {
            throw new FileException($exception->getMessage());
        }
    }


    /**
     * @param string $data
     * @return array|null
     */
    public static function getArgumentsByData(string $data): ?array
    {
        $pattern = '#(([0-9\-\+\x\*\/]+)\s{1}([0-9\-\+\x\*\/]+))#m';
        preg_match_all($pattern, $data, $arguments);
        $args = [];
        foreach ($arguments[2] as $key => $argument) {
            $args[] = [$arguments[2][$key], $arguments[3][$key]];
        }
        return $args;
    }

    /**
     * @param $file
     * @param $content
     * @return bool
     * @throws FileException
     */
    public static function save($file, $content): bool
    {
        $fileOpen = fopen($file, 'w') or die("не удалось создать файл");
        $r = fwrite($fileOpen, $content);
        fclose($fileOpen);
        return $r;
    }
}