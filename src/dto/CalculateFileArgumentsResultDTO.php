<?php
/**
 * @author Jakhar <jakharbek@gmail.com>
 * @package a-calc
 */

namespace jakharbek\aCalcTestLib\dto;

/**
 * Class CalculateFileArgumentsResultDTO
 * @package jakharbek\aCalcTestLib\dto
 */
class CalculateFileArgumentsResultDTO
{
    public $operation;
    public $arguments = [];
    public $resultAll = [];
    public $resultPositive = [];
    public $resultNegative = [];
}