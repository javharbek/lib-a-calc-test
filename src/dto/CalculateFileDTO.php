<?php
/**
 * @author Jakhar <jakharbek@gmail.com>
 * @package a-calc
 */

namespace jakharbek\aCalcTestLib\dto;

/**
 * Class CalculateFileDTO
 * @package jakharbek\aCalcTestLib\dto
 */
class CalculateFileDTO
{
    public $file;
    public $operation;
    public $savePath = null;
}