<?php
/**
 * @author Jakhar <jakharbek@gmail.com>
 * @package a-calc
 */

namespace jakharbek\aCalcTestLib\dto;

/**
 * Class generateFileContentResultDTO
 * @package jakharbek\aCalcTestLib\dto
 */
class generateFileContentResultDTO
{
    public $contentAll = "";
    public $contentNegative = "";
    public $contentPositive = "";
    public $content = "";
}