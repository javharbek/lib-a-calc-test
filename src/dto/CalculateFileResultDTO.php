<?php
/**
 * @author Jakhar <jakharbek@gmail.com>
 * @package a-calc
 */

namespace jakharbek\aCalcTestLib\dto;

/**
 * Class CalculateFileDTO
 * @package jakharbek\aCalcTestLib\dto
 */
class CalculateFileResultDTO
{
    public $files;
}