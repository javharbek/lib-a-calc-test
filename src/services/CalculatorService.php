<?php
/**
 * @author Jakhar <jakharbek@gmail.com>
 * @package a-calc
 */

namespace jakharbek\aCalcTestLib\services;


use jakharbek\aCalcTestLib\classes\Calculator;
use jakharbek\aCalcTestLib\dto\CalculateFileArgumentsResultDTO;
use jakharbek\aCalcTestLib\dto\CalculateFileDTO;
use jakharbek\aCalcTestLib\dto\CalculateFileResultDTO;
use jakharbek\aCalcTestLib\dto\generateFileContentResultDTO;
use jakharbek\aCalcTestLib\exceptions\CalculatorException;
use jakharbek\aCalcTestLib\helpers\FileHelper;
use jakharbek\aCalcTestLib\interfaces\CalculatorInterface;
use jakharbek\aCalcTestLib\interfaces\CalculatorServiceInterface;

/**
 * Class CalculatorService
 * @package jakharbek\aCalcTestLib\services
 *
 *
 * @example ```php
    $service = new CalculatorService();
    $dto = new CalculateFileDTO();
    $dto->file = 'test/test.txt';
    $dto->operation = "+";
    $dto->savePath= 'test/';
    $files = $service->calculateFile($dto);
 ```php
 *
 * The main business logic of processing, obtaining and saving the result.
 */
class CalculatorService implements CalculatorServiceInterface
{
    /**
     * @var CalculatorInterface
     */
    private $calculator;

    /**
     * CalculatorService constructor.
     */
    public function __construct()
    {
        $this->calculator = new Calculator();
    }

    /**
     * @param CalculateFileDTO $calculateFileDTO
     * @return CalculateFileResultDTO
     * @throws CalculatorException
     * @throws \jakharbek\aCalcTestLib\exceptions\FileException
     *
     * Opens the file and the file is received.
     * The result is processed.
     * The result is saved to a folder in the file with the results and positive and negative.
     */
    public function calculateFile(CalculateFileDTO $calculateFileDTO): CalculateFileResultDTO
    {
        $dto = new CalculateFileResultDTO();
        $data = FileHelper::getData($calculateFileDTO->file);
        $arguments = FileHelper::getArgumentsByData($data);
        $result = $this->calculateFileArguments($calculateFileDTO->operation, ...$arguments);
        $contentDTO = $this->generateFileContent($result);
        $saveResult = $this->save($contentDTO, $calculateFileDTO->savePath);
        $dto->files = $saveResult;
        return $dto;
    }

    /**
     * @param $operation
     * @param mixed ...$arguments
     * @return CalculateFileArgumentsResultDTO
     *
     * Accepts a file from which to read data and returns the result of the data.
     */
    public function calculateFileArguments($operation, ...$arguments): CalculateFileArgumentsResultDTO
    {
        $resultDTO = new CalculateFileArgumentsResultDTO();
        $resultDTO->operation = $operation;
        $resultDTO->arguments = $arguments;
        foreach ($arguments as &$argument) {
            $resultDTO->resultAll[] = $argument['result'] = $this->calculator->calc($operation, ...$argument);
            ($argument['result'] < 0) ? $resultDTO->resultNegative[] = $argument['result'] : $resultDTO->resultPositive[] = $argument['result'];
        }
        return $resultDTO;
    }

    /**
     * @param CalculateFileArgumentsResultDTO $argumentsResultDTO
     * @return generateFileContentResultDTO
     *
     * Receives the result of data processing and creates content for saving already in the file.
     */
    public function generateFileContent(CalculateFileArgumentsResultDTO $argumentsResultDTO): generateFileContentResultDTO
    {
        $dto = new generateFileContentResultDTO();
        $resultsAll = $argumentsResultDTO->resultAll;
        asort($resultsAll);
        $resultPositive = $argumentsResultDTO->resultPositive;
        asort($resultPositive);
        $resultNegative = $argumentsResultDTO->resultNegative;
        asort($resultNegative);
        $dto->contentAll = implode('
', $resultsAll);

        $dto->contentNegative = implode('
', $resultNegative);

        $dto->contentPositive = implode('
', $resultPositive);


        $dto->content = "-------------------  negative results -------------------
";
        $dto->content .= $dto->contentNegative;
        $dto->content .= "
-------------------  positive results -------------------
";
        $dto->content .= $dto->contentPositive;
        return $dto;
    }

    /**
     * @param generateFileContentResultDTO $contentResultDTO
     * @param null $path
     * @return array
     * @throws CalculatorException
     * @throws \jakharbek\aCalcTestLib\exceptions\FileException
     *
     * Gets content for saving results to files.
     */
    public function save(generateFileContentResultDTO $contentResultDTO, $path = null): ?array
    {
        if ($path == null) {
            throw new CalculatorException("Path for save result is empty");
        }
        $time = date("d-m-Y-H-i-s");
        $resultAllFilename = $path . "/".$time."-result_all.txt";
        $resultNegativeFilename = $path . "/" . $time . "-result_negative.txt";
        $resultPositiveFilename = $path . "/" . $time . "-result_positive.txt";
        FileHelper::save($resultAllFilename, $contentResultDTO->content);
        FileHelper::save($resultNegativeFilename, $contentResultDTO->contentNegative);
        FileHelper::save($resultPositiveFilename, $contentResultDTO->contentPositive);
        return [$resultAllFilename, $resultNegativeFilename, $resultPositiveFilename];
    }

}