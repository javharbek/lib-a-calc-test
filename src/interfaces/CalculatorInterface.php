<?php
/**
 * @author Jakhar <jakharbek@gmail.com>
 * @package a-calc
 */

namespace jakharbek\aCalcTestLib\interfaces;

/**
 * Interface CalculatorInterface
 * @package jakharbek\aCalcTestLib\interfaces
 *
 * The interface must implement standard arithmetic operations.
 */
interface CalculatorInterface
{
    const ADD_OPERATION = ["+"];
    const SUBTRACT_OPERATION = ["-"];
    const MULTIPLY_OPERATION = ['*','x','X'];
    const DEVIDE_OPERATION = ["/"];

    /**
     * @param array ...$operands
     * @return float
     *
     * The result of applying an arithmetic operation adding (+) to the function arguments.
     */
    public function add(...$operands): float;

    /**
     * @param array ...$operands
     * @return float
     *
     * The result is the application of the arithmetic operation subtraction (-) to the function arguments.
     */
    public function subtract(...$operands): float;

    /**
     * @param array ...$operands
     * @return float
     *
     * The result is the application of the arithmetic multiplication operation (x) to the function arguments.
     */
    public function multiply(...$operands): float;

    /**
     * @param array ...$operands
     * @return float
     *
     * The result is the application of the arithmetic division (/) operation to the function arguments.
     */
    public function divide(...$operands): float;

    /**
     * @param $operation
     * @param array ...$operands
     * @return float
     *
     * All operations in one method for quick access
     */
    public function calc($operation,...$operands):float;
}