<?php
/**
 * @author Jakhar <jakharbek@gmail.com>
 * @package a-calc
 */

namespace jakharbek\aCalcTestLib\interfaces;

use jakharbek\aCalcTestLib\dto\CalculateFileArgumentsResultDTO;
use jakharbek\aCalcTestLib\dto\CalculateFileDTO;
use jakharbek\aCalcTestLib\dto\CalculateFileResultDTO;
use jakharbek\aCalcTestLib\dto\generateFileContentResultDTO;
use jakharbek\aCalcTestLib\exceptions\CalculatorException;

/**
 * Interface CalculatorServiceInterface
 * @package jakharbek\aCalcTestLib\interfaces
 *
 * The main business logic of processing, obtaining and saving the result.
 */
interface CalculatorServiceInterface
{

    /**
     * @param CalculateFileDTO $calculateFileDTO
     * @return CalculateFileResultDTO
     * @throws CalculatorException
     * @throws \jakharbek\aCalcTestLib\exceptions\FileException
     *
     * Opens the file and the file is received.
     * The result is processed.
     * The result is saved to a folder in the file with the results and positive and negative.
     */
    public function calculateFile(CalculateFileDTO $calculateFileDTO): CalculateFileResultDTO;

    /**
     * @param $operation
     * @param mixed ...$arguments
     * @return CalculateFileArgumentsResultDTO
     *
     * Accepts a file from which to read data and returns the result of the data.
     */
    public function calculateFileArguments($operation, ...$arguments): CalculateFileArgumentsResultDTO;


    /**
     * @param CalculateFileArgumentsResultDTO $argumentsResultDTO
     * @return generateFileContentResultDTO
     *
     * Receives the result of data processing and creates content for saving already in the file.
     */
    public function generateFileContent(CalculateFileArgumentsResultDTO $argumentsResultDTO): generateFileContentResultDTO;


    /**
     * @param generateFileContentResultDTO $contentResultDTO
     * @param null $path
     * @return array
     * @throws CalculatorException
     * @throws \jakharbek\aCalcTestLib\exceptions\FileException
     *
     * Gets content for saving results to files.
     */
    public function save(generateFileContentResultDTO $contentResultDTO, $path = null): ?array;
}