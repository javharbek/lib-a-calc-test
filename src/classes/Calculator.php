<?php
/**
 * @author Jakhar <jakharbek@gmail.com>
 * @package a-calc
 */

namespace jakharbek\aCalcTestLib\classes;

use jakharbek\aCalcTestLib\exceptions\CalculatorException;
use jakharbek\aCalcTestLib\interfaces\CalculatorInterface;

/**
 * Class Calculator
 * @package jakharbek\aCalcTestLib\classes
 *
 * Standard arithmetic operations.
 */
class Calculator implements CalculatorInterface
{

    /**
     * @param array ...$operands
     * @return float
     * @throws CalculatorException
     *
     * The result of applying an arithmetic operation adding (+) to the function arguments.
     */
    public function add(...$operands): float
    {
        $this->checkOperands($operands);
        return (float)array_sum($operands);
    }

    /**
     * @param array ...$operands
     * @return float
     * @throws CalculatorException
     *
     * The result is the application of the arithmetic operation subtraction (-) to the function arguments.
     */
    public function subtract(...$operands): float
    {
        $this->checkOperands($operands);
        $result = 0;
        foreach ($operands as $operand) {
            $result = $operand - $result;
        }
        return (float)$result;
    }

    /**
     * @param array ...$operands
     * @return float
     * @throws CalculatorException
     *
     * The result is the application of the arithmetic multiplication operation (x) to the function arguments.
     */
    public function multiply(...$operands): float
    {
        $this->checkOperands($operands);
        $result = 1;
        try {
            foreach ($operands as $operand) {
                $result = $operand * $result;
            }
        } catch (\Exception $exception) {
            throw new CalculatorException($exception->getMessage());
        }
        return (float)$result;

    }

    /**
     * @param array ...$operands
     * @return float
     * @throws CalculatorException

     * The result is the application of the arithmetic division (/) operation to the function arguments.
     */
    public function divide(...$operands): float
    {
        $this->checkOperands($operands);
        $result = 1;
        try {
            foreach ($operands as $operand) {
                if ($result > 1) {
                    $result = $result / $operand;
                    continue;
                }
                $result = $operand / $result;
            }
        } catch (\Exception $exception) {
            throw new CalculatorException($exception->getMessage());
        }
        return $result;
    }

    /**
     * @param $operation
     * @param array ...$operands
     * @return float
     * @throws CalculatorException
     *
     * All operations in one method for quick access
     */
    public function calc($operation, ...$operands): float
    {
        $operation_method = $this->getOperationName($operation);
        return $this->$operation_method(...$operands);
    }


    /**
     * @param $operands
     * @throws CalculatorException
     *
     * Operand Check
     */
    private function checkOperands($operands)
    {
        if (!(count($operands) > 1)) {
            throw new CalculatorException("There must be more than two operands - you entered an incorrect number of arguments.");
        }

    }

    /**
     * @param string $operation
     * @return string
     * @throws CalculatorException
     *
     * Getting operation name
     */
    private function getOperationName(string $operation): string
    {
        switch ($operation) {
            case in_array($operation, self::ADD_OPERATION) :
                return "add";
            case in_array($operation, self::SUBTRACT_OPERATION) :
                return "subtract";
            case in_array($operation, self::MULTIPLY_OPERATION) :
                return "multiply";
            case in_array($operation, self::DEVIDE_OPERATION) :
                return "divide";
        }

        throw new CalculatorException("Operation is not founded");
    }
}