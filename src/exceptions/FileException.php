<?php
/**
 * @author Jakhar <jakharbek@gmail.com>
 * @package a-calc
 */

namespace jakharbek\aCalcTestLib\exceptions;

/**
 * Class FileException
 * @package jakharbek\aCalcTestLib\exceptions
 */
class FileException extends \Exception
{

}