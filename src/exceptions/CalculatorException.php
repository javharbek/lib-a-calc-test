<?php
/**
 * @author Jakhar <jakharbek@gmail.com>
 * @package a-calc
 */

namespace jakharbek\aCalcTestLib\exceptions;

/**
 * Class CalculatorException
 * @package jakharbek\aCalcTestLib\exceptions
 */
class CalculatorException extends \Exception
{

}