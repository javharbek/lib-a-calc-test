A-Calc-Lib
============ 
A-Calc-Lib

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer require --prefer-dist jakharbek/lib-a-calc-test "dev-master"
```

or add

```
"jakharbek/lib-a-calc-test": "dev-master"
```

to the require section of your `composer.json` file.


Usage
-----

This library retrieves data from a file for combing over each line a certain arithmetic operation and writing the result into a separate file.

Example
---
```php
$service = new CalculatorService();
$dto = new CalculateFileDTO();
$dto->file = 'test/test.txt';
$dto->operation = "+";
$dto->savePath= 'test/';
$files = $service->calculateFile($dto);
```
 
DIRECTORY STRUCTURE
-------------------

```
src
    classes/             contains classes
    dto/                 contains dto (data transfer objects)
    exceptions/          contains exceptions
    helpers/             contains helpers
    interfaces/          contains interfaces
    services/            contains services
```
